package com.company;

/**
 * Created by Oleg on 27.02.2017.
 */
public class Task {

    public static void main(String[] args) {

        int a = 7;
        int b = 7;

        if (a > b) {
            System.out.println("max = " + a);
        } else if (b > a) {
            System.out.println("max = " + b);
        }

        if (a < b) {
            System.out.println("min = " + a);
        } else if (b < a) {
            System.out.println("min = " + b);
        }

        if (a == b) System.out.println("Two values are equal");
    }
}