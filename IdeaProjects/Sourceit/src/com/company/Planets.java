package com.company;

/**
 * Created by Oleg on 28.02.2017.
 */
public class Planets {
    public static void main(String[] args) {
        int Mercury = 91691000;
        int Venus = 41400000;
        int Mars = 78340000;
        int Jupiter = 628730000;
        int Saturn = 1275000000;
        System.out.println("Distance from Mercury to Earth = " + Mercury + " km");
        System.out.println("Distance from Venus to Earth = " + Venus + " km");
        System.out.println("Distance from Mars to Earth = " + Mars + " km");
        System.out.println("Distance from Jupiter to Earth = " + Jupiter + " km");
        System.out.println("Distance from Saturn to Earth = " + Saturn + " km");


    }
}
