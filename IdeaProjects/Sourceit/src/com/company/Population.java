package com.company;

/**
 * Created by Oleg on 28.02.2017.
 */
public class Population {
    public static void main(String[] args) {
        int France = 64847000;
        int Germany = 80636124;
        int Italy = 59797978;
        int Spain = 46070146;
        int Ukraine = 44405055;
        System.out.println("France population = " + France + " people");
        System.out.println("German population = " + Germany + " people");
        System.out.println("Italian population = " + Italy + " people");
        System.out.println("Spanish population = " + Spain + " people");
        System.out.println("Ukrainian population = " + Ukraine + " people");
    }
}
