package com.company;

/**
 * Created by Oleg on 01.03.2017.
 */
public class Currency {
    public static void main(String[] args) {
        float Currency = 5000;
        float USD = 27.291f;
        float EUR = 28.953f;
        float RUB = 0.474f;
        float GBP = 34.105f;
        System.out.println("In USD you have = " + Currency / USD);
        System.out.println("In EUR you have = " + Currency / EUR);
        System.out.println("In RUB you have = " + Currency / RUB);
        System.out.println("In GBP you have = " + Currency / GBP);
    }
}
